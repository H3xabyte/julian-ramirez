from bs4 import BeautifulSoup as bs
import requests

URL = 'https://shorttracker.co.uk/'
LOGIN_ROUTE = 'accounts/login/'

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'origin': URL, 'referer': URL + LOGIN_ROUTE}

s = requests.session()

csrf_token = s.get(URL).cookies['csrftoken']
user = 'alexander.forero'
password = 'sad'

login_payload = {
        'login': user,
        'password': password, 
        'csrfmiddlewaretoken': csrf_token
        }

login_req = s.post(URL + LOGIN_ROUTE, headers=HEADERS, data=login_payload)

print(login_req.status_code)

cookies = login_req.cookies

soup = bs(s.get(URL + 'watchlist').text, 'html.parser')
tbody = soup.find('table', id='companies')
print(tbody)
