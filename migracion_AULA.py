from bs4 import BeautifulSoup as bs
import requests
import re
import shutil
import os
import time
import logging
from urllib.parse import urlparse
logger = logging.getLogger('try_Log')
curso1        = input("Introduce el curso a exportar (viejo SIE)")
curso2         = input("Introduce el curso a actualizar(AVE)")
URL = 'https://sie.educar.com.co/cas/login?service=http%3A%2F%2Fsie.educar.com.co%2F'
LOGIN_ROUTE = ''
URLCREARZIP = 'http://sie.educar.com.co:8080/LMS/main/coursecopy/create_backup.php?cidReq={}&id_session=0&gidReq=0&gradebook=0&origin='.format(curso1)
HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'origin': URL, 'referer': URL + LOGIN_ROUTE}

s = requests.session()
soup = bs(s.get('https://sie.educar.com.co/cas/login?service=http%3A%2F%2Fsie.educar.com.co%2Fsie%2F').text, 'html.parser')
soup.find('input', {"name":"lt"})
sp = soup.find('input', {"name":"lt"})
cas_token = sp['value']
print(cas_token)

user = 'alexander.forero'
password = 'alexander.forero'


login_payload = {
        'lt': cas_token,
        'execution': 'e1s1',
        '_eventId': 'submit',
        'username': user,
        'password': password, 
        'submit': 'ENTRAR'
        }

login_req = s.post(URL, headers=HEADERS, data=login_payload)
print(login_req.status_code)

cookies = login_req.cookies

r = s.get('https://sie.educar.com.co/cas/login?service=http%3A%2F%2Fsie.educar.com.co:8080%2FLMS%2Findex.php%2Fgateway=true')
soup = bs(s.get('http://sie.educar.com.co:8080/LMS/main/admin/course_list.php').text, 'html.parser')
soup.find_all('a')
for link in soup.find_all('a'):
    print(link.get('href'))
tbody = soup.find('table', id='companies')

""" print(login_req.status_code) """

""" print(tbody) """
##ya esta el login a el aula y a CAS, ahora sigue descargar el ZIP
## Obtengo el TOKEN de creacion de zip y procedo a crear el zip
soup2 = bs(s.get('http://sie.educar.com.co:8080/LMS/main/coursecopy/create_backup.php?cidReq={}&id_session=0&gidReq=0'.format(curso1)).text, 'html.parser')
soup2.find('input', {"name":"sec_token"})
sp2 = soup2.find('input', {"name":"sec_token"})
down_token = sp2['value']
print(down_token)

crearzip_payload = {
        'backup_option': 'full_backup',
        'submit': '',
        '_qf__create_backup_form': '',
        'sec_token': down_token
      
        }

crearzip_req = s.post(URLCREARZIP, headers=HEADERS, data=crearzip_payload)   
soup3 = bs((crearzip_req).text,'html.parser')   
soup3.find('a', {"class":"btn btn-primary btn-large"})
sp3 = soup3.find('a', {"class":"btn btn-primary btn-large"}) 
download_link = sp3['href']
print(download_link)
url = download_link
soup4 = s.get(url)
archivo = curso1 + '.zip'
fd = open(archivo, 'wb')
fd.write(soup4.content)
fd.close()
#### HASTA ESTE PUNTO, YA HEMOS DESCARGADO EL ZIP Y AHORA PROCEDEMOS A IMPORTARLO DENTRO DE EL AULA


URL = 'https://aula.educar.com.co/'
LOGIN_ROUTE = ''
URL_IMPORT_CURSOS = 'https://aula.educar.com.co/main/coursecopy/import_backup.php?cidReq={}&id_session=0&gidReq=0&gradebook=0&origin='.format(curso2)

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'origin': URL, 'referer': URL + LOGIN_ROUTE}

s = requests.session()

""" csrf_token = s.get(URL).cookies['csrftoken'] """
user = 'admin'
password = '$%EDUC4R2020.it'

login_payload = {
        'login': user,
        'password': password, 
        
        }

login_req = s.post(URL, headers=HEADERS, data=login_payload)

print(login_req.status_code)

cookies = login_req.cookies
r = s.get('https://aula.educar.com.co/main/admin/course_list.php')
soup = bs(s.get('https://aula.educar.com.co/main/admin/course_list.php').text, 'html.parser')
soup.find_all('a')
for link in soup.find_all('a'):
    print(link.get('href'))
tbody = soup.find('table', id='companies')
files = {'backup': open(archivo, 'rb')}
soup2 = bs(s.get(URL_IMPORT_CURSOS).text, 'html.parser')
soup2.find('input', {"name":"sec_token"})
sp2 = soup2.find('input', {"name":"sec_token"})
down_token = sp2['value']
print(down_token)

curso_payload = {
'backup_type': 'local',
'import_option' : 'full_backup',
'same_file_name_option': '3',
'_qf__import_backup_form': '',
'action': 'restore_backup',
'MAX_FILE_SIZE': '1048576000',  
'sec_token': down_token,  

 }

curso_req = s.post(URL_IMPORT_CURSOS, files=files, headers=HEADERS, data=curso_payload)

print(curso_req.status_code)
print('OK CURSO ACTUALIZADO SATISFACTORIAMENTE')
cookies = curso_req.cookies