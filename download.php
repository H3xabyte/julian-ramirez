


	






<html lang="es">

<head>
  <meta charset="utf-8">
  <title>SIE | Solución integral educativa</title>
  <meta name="description"
    content="Poderosa herramienta que integra los beneficios pedagógicos de los textos escolares y la efectividad en el aprendizaje de nuevas tecnologías.">
  <meta name="keywords"
    content="recursos educativos, matemáticas, ingles, sociales, fisica, quimica, ciencias naturales, lecciones, lms, textos escolares, comunidad">
  <meta name="news_keywords"
    content="recursos educativos, matemáticas, ingles, sociales, fisica, quimica, ciencias naturales, lecciones, lms, textos escolares, comunidad">
  <meta name="robots" content="index">
  <meta name="author" content="EDUCAR">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="geo.placename" content="Colombia">
  <meta name="geo.region" content="CO">
  <meta property="og:type" content="website">
  <meta property="og:title" content="SIE | Solución integral educativa">
  <meta property="og:description"
    content="Poderosa herramienta que integra los beneficios pedagógicos de los textos escolares y la efectividad en el aprendizaje de nuevas tecnologías.">
  <meta property="og:url" content="https://sie.educar.com.co">
  <meta property="og:image" content="http://sie.educar.com.co/resources/imagen/SIE_EDUCAR.png">
  <meta property="og:site_name" content="SIE | Solución integral educativa">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@SIE">
  <meta name="twitter:creator" content="@educar">
  <meta name="twitter:title" content="SIE | Solución integral educativa">
  <meta name="twitter:description"
    content="Poderosa herramienta que integra los beneficios pedagógicos de los textos escolares y la efectividad en el aprendizaje de nuevas tecnologías.">
  <meta name="twitter:image" content="http://sie.educar.com.co/resources/imagen/SIE_EDUCAR.png">
  <link href="https://sie.educar.com.co" rel="canonical">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="shortcut icon" href="/cas/images/sie/favicon.ico" type="image/vnd.microsoft.icon" />

  <!-- jQuery http://jquery.com Ver 1.12.0-->
  <!--[if lt IE 9]>
            <script src="/cas/js/jquery-1.js"></script>
            <![endif]-->
  <!--[if gte IE 9]><!-->
  <script src="/cas/js/jquery-2.js"></script>
  <!--<![endif]-->

  <!-- jQuery UI Support http://jqueryui.com Ver 1.11.4-->
  <!--  -->

  <script>
    (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-69428950-1', 'auto');
    ga('send', 'pageview');

  </script>
  <!-- <script src="/cas/js/jquery-ui.js" type="text/javascript"></script>
  <script src="/cas/css/sieui/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="/cas/css/sieui/jquery-ui.min.css" id="theme"> -->
  <link rel="stylesheet" href="/cas/css/newsie/bootstrapSIE.css" id="theme">
  <link rel="stylesheet" href="/cas/css/newsie/sie.css" id="theme">
  <link rel="stylesheet" href="/cas/css/newsie/login.css" id="theme">
  <link rel="stylesheet" href="/cas/css/NewPlataforma/sie/css/main.css" id="theme">
  <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
  <!-- jQuery Social Locker Ver 1.5.2
            <link rel="stylesheet" href="/cas/css/jquery.onp.sociallocker.min.css" id="theme">
            <script src="/cas/js/jquery.onp.sociallocker.min.js" type="text/javascript"></script>
            -->

  <noscript>
    <style type="text/css">
      #wrapper {
        display: none;
      }
    </style>
    <div class="noscriptmsg">

      Estimado usuario su navegador tiene deshabilitado el
      <i>javascript</i>, este sistema no puede funcionar con esta tecnología deshabilitada.
      <br />
      Por favor habilitela y vuelva a cargar el sitio.
    </div>
  </noscript>

  <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  
  
</head>
	
<body >
	
	<div id="loadPruebasSaber" style="width: 100%;height: 100%; min-height: 500px;background: #fff;justify-content: center; flex-direction: column; display: none; position: absolute; z-index: 9999999;">
		<img class="img-brand" src="http://sie.educar.com.co/Saber2019/assets/img/saber.png" alt="saber"
			style="height: 64px !important; margin: 0 auto;">
		<h2 style="color: #444; text-align: center;padding-top: 50px;">
			Cargando...
		</h2>
	</div>
	<div id="content_sie_login">
		
		<!-- <h1>Estamos realizando un mantenimiento del sistema para garantizar la calidad del servicio. <br> Regresamos en pocos minutos</h1> -->
		<nav class="navbar navbar-default navbar-fixed-top" id="top">
			<div class="container">
				<div class="navbar-header logo">
						<div class="navbar-brand">
							<a href="//sie.educar.com.co"><img class="img-brand" src="/cas/images/logosie2016.png" alt=""></a>
						</div>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li><a class="link-ayuda" target="_blank" href="http://sie.educar.com.co/soporte.php">¿Necesitas ayuda?</a></li>
					<li><a class="inner" href="http://sie.educar.com.co/activar.php">ACTIVA TU PIN</a></li>
				</ul>
			</div>
		</nav>
		<!-- Modulo login -->
	<div class="container" id="cambiante">
		<div class="row" >
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<div class="form-horizontal">
					
					<form id="fm1" class="fm-v clearfix" action="/cas/login;jsessionid=2B6E43BCBF69B87E7160E04313026632?service=http%3A%2F%2Fsie.educar.com.co%2F" method="post">
					<input type="hidden" name="lt" value="LT-20144-sY3Yzre9PJvt5Qao62Q1xf71sy3Umu"/>
					<input type="hidden" name="execution" value="e1s1"/>
					<input type="hidden" name="_eventId" value="submit"/>
					<div class="input-group img_info">
						<a href="http://sie.educar.com.co/soporte.php"><img style="float: right;" src="/cas/css/NewPlataforma/sie/images/ayuda.png"></a>
					</div>
					
					<div class="input-group">
								
								
									
									<input id="username" name="username" class="required form-control" tabindex="1" placeholder="USUARIO" accesskey="n" type="text" value="" size="20" autocomplete="false"/>
								
					</div>
					<div class="input-group">
						
						<input id="password" name="password" class="required form-control" tabindex="2" placeholder="CONTRASE&Ntilde;A" accesskey="c" type="password" value="" size="20" autocomplete="off"/>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-1 col-sm-10">
						<input type="submit" id="intoEV" class="entrar btn btn-primary btn-lg" name="submit" accesskey="l" value="ENTRAR" tabindex="4"/>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-1 col-sm-10">
							
							<a target="_blank" href="http://sie.educar.com.co/clave.php"><button type="button" class="btn btn-primary btn-occ">RECUPERA TU CONTRASEÑA</button></a>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
		</div>
		
<style media="screen">
@media(max-width: 767px){
	.clNewHour{
		position: relative !important;
		top: 0 !important;
	}
	.clNewHour div{
		display: flex;
		flex-direction: row;
		justify-content: center;
	}
	.clNewHour h5{
		display: block;
	}
}

.clNewHour{
	position: absolute;
	top: 45px;
}

.clNewHour div{
	padding: 0px;
	min-width: 200px;
	height: 20px;
	text-align: justify !important;
}
.clNewHour h5{
	float: left;
}
.clNewHour h4{
	float: right;
}

</style>
		<link rel="stylesheet" href="/cas/css/NewPlataforma/footer.css">
	     <footer class="footer">
          <div class="container">
            <div class="row">
              <div class="col-sm-3 col-xs-12 footer_t1 text-center">
				  <a class="logo_footer_educar"><img src="/cas/images/footer/logo_educar.png"></a>
              </div>
              <div class="col-sm-3 col-xs-12 footer_t2">
				  <h4><img id="telefono" src="/cas/images/footer/telefono.png">CANALES DE SOPORTE</h4>
				  <h5>Correo: sie@educar.com.co</h5>
				  <h5>PBX: (1) 876 4301</h5>
				  <h5>Línea gratuita: 01 8000 184 315</h5>
				  <h5>Celular: 320 858 3903</h5>
				  <h5>Whatsapp: 300 504 1639</h5>
              </div>
              <div class="col-sm-3 col-xs-12 footer_t3">
	    			  <h4><img id="relog" src="/cas/images/footer/relog.png">HORARIO DE ATENCIÓN</h4>
							<div class="row clNewHour">
								<div class="col-sm-12">
										<h5 style="color: #fff !important" >De lunes a viernes de: 7:00am a 5:00pm</h5>
										<h4 style="color: #fff !important; float: left;"></h4>
										 
									
									</div>

								<!-- <div class="col-sm-12">
									<h5>Martes:</h5>
									<h4>7:00 a.m - 8:00 p.m</h4>
								</div>

								<div class="col-sm-12" >
									<h5 style="color: #999 !important">Miércoles:</h5>
									<h4 style="color: #999 !important">7:00 a.m - 5:00 p.m</h4>
								</div>

								<div class="col-sm-12">
									<h5>Jueves:</h5>
									<h4>7:00 a.m - 8:00 p.m</h4>
								</div>

								<div class="col-sm-12">
									<h5>Viernes:</h5>
									<h4>7:00 a.m - 8:00 p.m</h4>
								</div> -->
							</div>
              </div>
              <div class="col-sm-3 col-xs-12 footer_t4 text-center">
				  <a target="_blank" href="https://www.educar.com.co/" class="img_footer_1"><img src="/cas/images/footer/link_b.png"></a>
				  <a target="_blank" href="https://www.facebook.com/educar.editores/" class="img_footer_2"><img class="img_1" src="/cas/images/footer/face_b.png"></a>
				  <a target="_blank" href="https://www.instagram.com/educareditores/" class="img_footer_3"><img class="img_1" src="/cas/images/footer/instagram_b.png"></a>
				  <a target="_blank" href="https://twitter.com/EducarEditores/" class="img_footer_4"><img class="img_1" src="/cas/images/footer/twiter_b.png"></a>
				  <h5>solicitudesit@educar.com.co</h5>
				  <h5 data-toggle="modal" data-target="#credits" style="cursor: pointer;">CRÉDITOS</h5>
              </div>
            </div>
            <div class="row derechos_mas text-center">
				<h5>Haz clic aquí y conoce nuestros <a href="http://sie.educar.com.co/terminos_y_condiciones.php" target="_blank" style="font-size: 16px;text-decoration: underline;">términos y
						condiciones</a> y la <a href="http://sie.educar.com.co/proteccion_de_datos.php" target="_blank" style="font-size: 16px;text-decoration: underline;">política de protección de datos
						personales</a></h5>
              <h5>Todos los derechos reservados y marcas registradas son propiedad del GRUPO EDITORIAL EDUCAR ©</h5>
            </div>
          </div>
        </footer>

		<!-- <div id="credits" class="modal fade" role="dialog" style="z-index: 10000;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" style="color: black;">CRÉDITOS</h4>
					</div>
					<div class="modal-body" style="color: black;font-size: 16px;">
						<ul>
							<li>
								Manuel Gil - Programador
							</li>
						</ul>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div> -->

<script>
$(document).ready(function() {
   $(".img_footer_1 img").hover(function() {
	     $(this).attr('src','/cas/images/footer/link.png');
   	   },function() {
		 $(this).attr('src','/cas/images/footer/link_b.png');
   });

   $(".img_footer_2 img").hover(function() {
	     $(this).attr('src','/cas/images/footer/face.png');
   	   },function() {
		 $(this).attr('src','/cas/images/footer/face_b.png');
   });

   $(".img_footer_3 img").hover(function() {
	     $(this).attr('src','/cas/images/footer/instagram.png');
   	   },function() {
		 $(this).attr('src','/cas/images/footer/instagram_b.png');
   });

   $(".img_footer_4 img").hover(function() {
	     $(this).attr('src','/cas/images/footer/twiter.png');
   	   },function() {
		 $(this).attr('src','/cas/images/footer/twiter_b.png');
   });

});
</script>


		<script src="/cas/images/newsie/js/bootstrap.js"></script>
		<script language="javascript">
			$(function() {				
				function cambiar_personaje(){
					
					var Imagenes = new VecImagenes();
					var r = Math.random();
					src1 = Imagenes[ Math.floor(r * Imagenes.N)] ;
					imagen = document.getElementById("cambiante");
					imagen.style.backgroundImage = "url(/cas/css/NewPlataforma/sie/images/"+src1+")"; 
				}

				function VecImagenes()
				{
					n=0;
					this[n++]="1.png";
					this[n++]="2.png";
					this[n++]="3.png";
					this[n++]="4.png";
					this[n++]="6.png";
					this[n++]="7.png";
					this.N=n;
				}
				
				setInterval(cambiar_personaje, 4000);
			});
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83857978-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-83857978-1');
	</script>
	</div>

</body>
</html>




 


	


	<script>
	$( "#username" ).on( "blur", function() {
		$( "#username" ).val( $( "#username" ).val().toLowerCase());
	});
	</script>




<style>
	.alert-warning {
		position: absolute;
		width: 100%;
		top: 101px;
		background-color: #fcf8e3;
		border-color: #faebcc;
		color: #8a6d3b;
		font-size: 15px;
	}
</style>
<div class="alert alert-warning" role="alert">
	<p>
		Les informamos que nuestros canales de soporte estarán disponibles en los siguientes horarios: <strong> Lunes a Viernes de 7:00am a 5:00pm </strong>.
	</p>
</div>
<!-- 
<script>
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
 <div id="myModal" class="modal fade">
    <div class="modal-dialog" style="margin-top: 166px;">
        <div class="modal-content" style="background-color: #0082b6;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">AVISO IMPORTANTE</h4>
            </div>
            <div class="modal-body">
				<img src="https://i.ibb.co/rkW3vKc/intermitencias-plataforma-18-marzo-1.png" alt="intermitencias-plataforma-18-marzo-1" style="width: 100%;">               
            </div>
        </div>
    </div>
</div> -->



